import readlineSync from 'readline-sync';

import Board from './Board';
import Rover from './Rover';
import Directions from './Directions';
import Rotation from "./Rotation";


// Generate the Grid
const boardQuery = readlineSync.question('Please enter the board size:');
const [ boardWidth, boardHeight ] = boardQuery.split(' ').map(Number);
const board = new Board(boardWidth, boardHeight);

console.log(board);
let a =0;

// rover query loop


while(true) {
    const roverQuery = readlineSync.question('Please type position and direction for Rover(x y d):');
    const [posX, posY, direction] = parseRoverQuery(roverQuery);
    board.addRover(posX, posY, direction);

    // rover navigation query
    const navigationQuery = readlineSync.question('Please type the navigation commands');


    if (a === 2 ) {
        break;
    }
}

function parseRoverQuery(query) {
    const roverValidator = /^\d+ \d+ [N|W|S|E]$/gm;

    if (!roverValidator.test(query)) {
        throw new Error('invalid query');
    }

    const queryArguments = query.split(' ');

    if (queryArguments.length !== 3) {
        throw new Error('Invalid argument count.');
    }

    let [posX, posY, direction] = query.split(' ');
    posX = parseInt(posX);
    posY = parseInt(posY);

    return [posX, posY, direction];
}

