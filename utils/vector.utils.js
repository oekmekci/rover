export function toRadians(angle) {
    return angle * (Math.PI / 180);
}

export function cosRounded(angle) {
    return Math.round(Math.cos(angle));
}

export function sinRounded(angle) {
    return Math.round(Math.sin(angle));
}

export function rotateClockwise(vector) {
    const [posX, posY] = vector;

    const theta = toRadians(-90);
    const cosTheta = cosRounded(theta);
    const sinTheta = sinRounded(theta);

    const newX = posX * cosTheta - posY * sinTheta;
    const newY = posX * sinTheta + posY * cosTheta;

    return [newX, newY];
}

export function rotateCounterClockwise(vector) {
    const [posX, posY] = vector;

    const theta = toRadians(90);
    const cosTheta = cosRounded(theta);
    const sinTheta = sinRounded(theta);

    const newX = posX * cosTheta - posY * sinTheta;
    const newY = posX * sinTheta + posY * cosTheta;

    return [newX, newY];
}
