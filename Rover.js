import Rotation from "./Rotation";
import { rotateClockwise, rotateCounterClockwise } from "./utils/vector.utils";

export default class Rover {
    constructor(position, direction) {
        this.position = position;
        this.direction = direction;
    }

    move() {
        const [posX, posY] = this.position;
        const [rotX, rotY] = this.direction;

        this.position = [posX + rotX, posY + rotY];
    }

    // 0 for counterclockwise 1 for clockwise
    rotate(targetDirection) {
        if(Object.values(Rotation).indexOf(targetDirection) === -1) {
            throw new Error('Invalid rotation direction');
        }

        switch (targetDirection) {
            case Rotation.LEFT:
                this.direction = rotateCounterClockwise(this.direction);
                break;
            case Rotation.RIGHT:
                this.direction = rotateClockwise(this.direction);
                break;
        }
    }

    rotateClockWise() {
        console.log(this, 'c');
        this.rotate(Rotation.RIGHT);
    }

    rotateCounterClockWise() {
        console.log(this, 'rotate cc');
        this.rotate(Rotation.LEFT);
    }

    execute(commandString) {
        const commandValidator = /^[L|R|M]+$/gm;

        if (!commandValidator.test(commandString)) {
            throw new Error('invalid commands')
        }

        const roverCommands = {
            R: this.rotateClockWise,
            L: this.rotateCounterClockWise,
            M: this.move
        };


        const commands = commandString.split('');
        commands.forEach(command => {
            const action = roverCommands[command].bind(this);
            action();
            console.log(this);
        })
    }
}
