import Rover from './Rover';
import Directions from "./Directions";

export default class Board {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.rovers = [];
    }

    addRover(posX, posY, direction) {
        const directions = {
            W: 'WEST',
            S: 'SOUTH',
            E: 'EAST',
            N: 'NORTH',
        };

        const directionWord = directions[direction];
        const directionPosition = Directions[directionWord];

        if (posX >= this.width) {
            throw new Error('Rover is out of the board on X axis');
        }

        if (posY >= this.height) {
            throw new Error('Rover is out of the board on Y axis');
        }

        const rover = new Rover([posX, posY], directionPosition);
        this.rovers.push(rover);
    }

    get activeRover() {
        return this.rovers[this.rovers.length -1];
    }
}
