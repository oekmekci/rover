'use strict';

import chai from 'chai';

const expect = chai.expect;

import Rover from '../Rover';

describe('Rover', function () {
  it('should initialize gracefully', function () {
    const rover = new Rover();
    expect(rover).to.be.instanceOf(Rover);
  })
});
