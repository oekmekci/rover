export default {
    WEST: [-1, 0],
    SOUTH: [-1, 0],
    EAST: [1, 0],
    NORTH: [0, 1]
};
